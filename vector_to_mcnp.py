from sys import argv, exit
from math import ceil, floor

if len(argv) == 1:
	exit("Please provide vector formatted file as argument")

string_list = []

with open(argv[1], 'r') as vector_file:
	for entry in vector_file.readlines():
		string_list.append(entry.rstrip('\n'))

max_entry_length = 0
for entry_index, entry in enumerate(string_list):
	if len(entry) > max_entry_length:
		max_entry_length = len(entry)

entries_per_line = floor(65/max_entry_length)

with open(argv[1].split('_')[0] + '_mcnp', 'w') as mcnp_file:
	for line in range(ceil(len(string_list)/entries_per_line)):
		energies_string = ' '.join(string_list[line*entries_per_line:(line + 1)*entries_per_line]) + '\n'
		if line == 0:
			mcnp_file.write(energies_string)
		else:
			mcnp_file.write(' '*5 + energies_string)
