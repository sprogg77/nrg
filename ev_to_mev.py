from sys import argv, exit

def char_index(target, string):
	for char_num, char in enumerate(string):
		if char == target:
			return char_num

if len(argv) != 2:
	exit("Please provide input eV vector file.")

with open(argv[1], 'r') as mev_file:
	mev_str_list = mev_file.readlines()

ev_list = []

sample_entry = mev_str_list[0]
precision_str = sample_entry[char_index('.', sample_entry) + 1:char_index('E', sample_entry)]
precision = len(precision_str)

for energy in mev_str_list:
	ev_list.append(float(energy)/1E6)

with open(argv[1] + '_MeV', 'w') as ev_file:
	for energy in ev_list:
		ev_file.write("{ENERGY:1.{PRECISION}E}\n".format(	ENERGY=energy,
																											PRECISION=precision))
