#!/bin/bash

if [ -n "$1" ]
then
	directory=$1
else
	directory=$PWD
fi

shopt -s nullglob
suffix=_mcnp

echo "Converting all vector format energy structures in:" 
echo "$PWD to <80 character, padded MCNP format."

for file in *_vector
do
	group_name=${file%_vector}
	echo "$file -> $group_name$suffix"
	python3 vector_to_mcnp.py $file
done
